<?
use \Lyrmin\Route, \Lyrmin\Menu, \Lyrmin\Auth, \Lyrmin\User, \Lyrmin\Application;

require_once ('settings.php');

if (INSTALL_FINISHED) {

	$USER = User\getCurrent();

    if (Application\isPost()) {
        $authorize = isset($_POST["authorize"]);
        $logout = isset($_POST["logout"]);

        if ($authorize && !Auth\isAuthorized()) {
			$USER = Auth\authorize(htmlspecialchars($_POST["login"]), htmlspecialchars($_POST["password"]));
        }

        if ($logout) {
            auth\logout();
        }
    }

    if (Auth\isAuthorized()) {
		// Добавляем страницы
		Route\add(['/route/', ['title' => 'Главная', 'html' => 'Route test html', 'sort' => '100']]);
		Route\add(['/route/contacts/', ['title' => 'Контакты', 'html' => 'Contacts test html', 'sort' => '400']]);
		Route\add(['/route/library/', ['title' => 'Библиотека', 'html' => 'Library test html', 'sort' => '200']]);
		Route\add(['/route/catalog/', ['title' => 'Каталог', 'html' => 'Catalog test html', 'sort' => '300']]);

		if (\Lyrmin\Group\getUserRights($USER["ID"]) > 'R') {
			Route\add([
				'/post/add/',
				[
					'title' => 'Добавить сообщение',
					'html' => require($_SERVER['DOCUMENT_ROOT'] . '/route/pages/post/add/index.php'),
					'sort' => '1100'
				]
			]);

			Route\add([
				'/post/view/',
				[
					'title' => 'Список сообщений',
					'html' => require($_SERVER['DOCUMENT_ROOT'] . '/route/pages/post/view/index.php'),
					'sort' => '1200'
				]
			]);
		}
	}

	Route\add([
		'/post/',
		[
			'title' => 'Сообщения',
			'html' => require($_SERVER['DOCUMENT_ROOT'] . '/route/pages/post/index.php'),
			'sort' => '1000'
		]
	]);

	Route\add([
		'/route/auth/',
		[
			'title' => Auth\isAuthorized() ? 'Выйти':'Авторизация',
			'html' => require($_SERVER['DOCUMENT_ROOT'] . '/route/pages/auth.php'),
			'sort' => '9000'
		]
	]);
} else {
    Route\add([
        '/route/install/',
        [
            'title' => 'Установка',
            'html' => require($_SERVER['DOCUMENT_ROOT'] . '/route/pages/install.php'),
            'sort' => '100'
        ]
    ]);
	Auth\pageRedirect('/route/install/');
}
Menu\menu(Route\getList());
$page = Route\getCurrent();

require ($_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php');