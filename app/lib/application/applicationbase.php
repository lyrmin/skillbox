<?php
/**
 * Created by PhpStorm.
 * User: las7
 * Date: 14.10.2018
 * Time: 14:18
 */

namespace Lyrmin\Application;

function getFilePath()
{
    return __FILE__;
}

function isPost()
{
	global $_SERVER;
	return $_SERVER["REQUEST_METHOD"] == "POST";
}

function setTitle($title = '')
{
    return (mb_strlen($title) > 15 ? mb_substr($title, 0, 11) . '...' : $title);
}

function getTemplate($lib = "", $class = "", $result)
{
    if (!strlen($lib) && !strlen($class)) {
        return;
    }
    $filePath = $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . "/blocks/" . $lib . '/' . $class . '/template.php';
    if (file_exists($filePath)) {
        require ($filePath);
    }
    return file_exists($filePath);
}

function pre($data)
{
    return $data ? "<pre>" . print_r($data, true) ."</pre>" : false;
}

