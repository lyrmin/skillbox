<?php
/**
 * Created by PhpStorm.
 * User: Анна
 * Date: 05.01.2019
 * Time: 21:30
 */

namespace Lyrmin\Auth;

use function Lyrmin\Application\pre;
use Lyrmin\User, Lyrmin\Group;

if (!is_session_started()) {
    ini_set('session.gc_maxlifetime', 20 * 60);
    ini_set('session.cookie_lifetime', 20 * 60);
    ini_set('session.cache_expire', 20 * 60);
    session_set_cookie_params(20 * 60);
    session_name("session_id");
    session_start();
}

function authorize($login = '', $password = '')
{
    if (empty($login) || empty($password)) {
        return false;
    }

	$password = md5($password . 'mysalt111');

    $USER = User\getList(['WHERE' => ['EMAIL' => $login, 'PASSWORD' => $password]]);

    if(!empty($USER)) {
		$USER = $USER[0];
    	// Получить права пользователя
		$USER['GROUP'] = Group\getUserGroups(['WHERE' => ['USER_ID' => $USER["ID"]]]);

    	setAuthorized();
	}

	return $USER;
}

function logout()
{
    unset($_SESSION["AUTHORIZED"]);
    session_destroy();
    pageRedirect();
}

function cookieTime($seconds = 60 * 60 * 24 * 32)
{
    return time() + $seconds;
}

function setAuthorized()
{
    global $_SESSION, $_POST;

    $_SESSION["AUTHORIZED"] = 'Y';

	if (isset($_POST["login"])) {
		setcookie("user", htmlspecialchars($_POST["login"]), cookieTime(60 * 20), "/");
	}

    return true;
}

function getLastLogin()
{
    global $_COOKIE;
    return isset($_COOKIE["user"]) ? $_COOKIE["user"] : false;
}

function isAuthorized()
{
    global $_SESSION, $_POST;

    if (!isset($_SESSION["AUTHORIZED"])) {

    	if (isset($_POST["another_user"])) {
			setcookie("user", '', 1, "/");
			unset($_COOKIE["user"]);
		}

    	//pageRedirect();
        return false;
    }

    return setAuthorized();
}

function pageRedirect($redirectTo = '/route/auth/')
{
    global $_SERVER;

    $uri = explode('?', $_SERVER["REQUEST_URI"])[0];
    if ($uri != $redirectTo) {
		header('Location: ' . $redirectTo);
		die();
    }
}

function is_session_started()
{
    if ( php_sapi_name() !== 'cli' ) {
        if ( version_compare(phpversion(), '5.4.0', '>=') ) {
            return session_status() === PHP_SESSION_ACTIVE ? true : false;
        } else {
            return session_id() === '' ? false : true;
        }
    }
    return false;
}
