<?

namespace Lyrmin\Db;

function getFilePath()
{
    return __FILE__;
}

/**
 * Подключение к БД
 * @param $dbHost
 * @param $dbName
 * @param $dbUser
 * @param $dbPass
 * @param $disconnect
 * @return \Exception|\PDO|\PDOException
 */
function connect($dbHost = '', $dbName = '', $dbUser = '', $dbPass = '', $disconnect = false)
{
	static $DB;

	if ($DB === null) {
		try {
			$dsn = "mysql:host=$dbHost;dbname=$dbName";
			$DB = new \PDO($dsn, $dbUser, $dbPass);
			$DB->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
			$DB->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);

			isConnected(true);
		} catch (\PDOException $e) {
			exit($e->getMessage());
		}
	}

	if ($disconnect === true) {
		$DB = null;
	}

    return $DB;
}

/**
 * Установка флага подключения/откулючения БД, получение текущего флага подключения
 * @param $set
 * @return mixed
 */
function isConnected($set) {
	static $connected;
	if (isset($set)) {
		$connected = $set;
	}
	return $connected;
}

function dbStatement($set = null) {
	static $statement;
	if (isset($set)) {
		$statement = $set;
	}
	return $statement;
}

/**
 * Отключение от БД
 * @return null
 */
function disconnect()
{
    connect('', '', '', '', true);
}

function init($query = '', $parameters = [])
{
	$DB = connect();

	try {
		$dbStatement = dbStatement($DB->prepare($query));
		$dbParameters = bind($parameters);
		if (!empty($dbParameters)) {
			foreach ($dbParameters as $param => $value) {
				if (is_int($value[1])) {
					$type = \PDO::PARAM_INT;
				} elseif (is_bool($value[1])) {
					$type = \PDO::PARAM_BOOL;
				} elseif (is_null($value[1])) {
					$type = \PDO::PARAM_NULL;
				} else {
					$type = \PDO::PARAM_STR;
				}
			}

			$dbStatement->bindValue($value[0], $value[1], $type);
		}

		$dbStatement->execute();

	} catch (\PDOException $e) {
		exit($e->getMessage());
	}

	bind([]);
	return $DB;
}

function bind($parameters = [])
{
	static $dbParameters;
	if (!empty($parameters) && is_array($parameters)) {
		$columns = array_keys($parameters);

		foreach ($columns as $i => &$column) {
			$dbParameters[sizeof($dbParameters)] = [
				':' . $column,
				$parameters[$column]
			];
		}
	}
	return $dbParameters;
}

function query($query = "", $parameters = [], $mode = \PDO::FETCH_ASSOC)
{
	$query = trim(str_replace(PHP_EOL, '', $query));

	$DB = init($query, $parameters);
	if ($DB !== null) {
		$dbStatement = dbStatement();

		$rawStatement = explode(' ', preg_replace("/\s+|\t+|\n+/", " ", $query));
		$statement = strtolower($rawStatement[0]);

		if ($statement === 'select' || $statement === 'show') {
			return $dbStatement->fetchAll($mode);
		} elseif ($statement === 'insert') {
			return lastInsertId();
		} elseif ($statement === 'update' || $statement === 'delete') {
			return $dbStatement->rowCount();
		} else {
			return null;
		}
	}
}

function lastInsertId()
{
	$DB = connect();
	return $DB->lastInsertId();
}