<?

namespace Lyrmin\Db;

function filterToQuery($sql = [])
{
	if (!is_array($sql) || empty($sql)) return;

	$query = '';

	foreach ($sql as $param => $arFields) {
		if (!is_array($arFields)) {

		} else {
			$arr = [];
			foreach ($arFields as $field => $value) {
				switch ($param) {
					case 'ORDER BY':
						if (is_array($value)) {
							$arr[] = '' . $field . ' in (' . implode(',', $value) . ')';
						} else {
							$arr[] = '' . $field . ' ' . $value . '';
						}
						break;
					default:
						if (is_array($value)) {
							$arr[] = '' . $field . ' in (' . implode(',', $value) . ')';
						} else {
							if (empty($value)) {
								$arr[] = '`' . $field . '` IS NULL';
							} else {
								$arr[] = '' . $field . '="' . $value . '"';
							}
						}
				}
			}
		}

		switch ($param) {
			case 'ORDER BY':
				if (!empty($arr)) {
					$query .= ' ' . $param .' ' . implode(', ', $arr);
				} elseif (!is_array($arFields)) {
					$query .= ' ' . $param . ' ' . $arFields;
				}
				break;
			default:
				if (!empty($arr)) {
					$query .= ' ' . $param .' ' . implode(' AND ', $arr);
				} elseif (!is_array($arFields)) {
					$query .= ' ' . $param . ' ' . $arFields;
				}
		}
	}

	return trim($query);
}