<?
namespace Lyrmin\Install;

use Lyrmin\Db;
/**
 * Записываем настройки подключения к БД в файл
 */
function doInstall()
{
	//echo \Lyrmin\Application\pre($_POST);
	if (
		isset($_POST["db_connect"])
		&&
		isset($_POST["db_host"])
		&&
		isset($_POST["db_name"])
		&&
		isset($_POST["db_user"])
		&&
		isset($_POST["db_password"])
	) {

		$dbHost = htmlspecialchars($_POST["db_host"]);
		$dbName = htmlspecialchars($_POST["db_name"]);
		$dbUser = htmlspecialchars($_POST["db_user"]);
		$dbPass = $_POST["db_password"];

		$data = '<?' . PHP_EOL;
		$data .= '$dbHost = "' . $dbHost .'";' . PHP_EOL;
		$data .= '$dbName = "' . $dbName .'";' . PHP_EOL;
		$data .= '$dbUser = "' . $dbUser .'";' . PHP_EOL;
		$data .= '$dbPass = "' . $dbPass .'";';

		$configFile = $_SERVER["DOCUMENT_ROOT"] . SITE_DIR . 'app/dbconnect.php';

		// Проверка подключения к БД
		if (Db\connect($dbHost, $dbName, $dbUser, $dbPass)) {
			file_put_contents($configFile, $data);

			foreach (['/mysql/install.sql', '/mysql/demodata.sql'] as $src) {
				$querySrc = trim(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $src));
				preg_match_all('/^[A-z]+(.+\n|\;)+/mu', $querySrc, $arQuery);
				$arQuery = $arQuery[0];

				if (is_array($arQuery) && !empty($arQuery)) {
					foreach ($arQuery as $query) {
						$query = trim($query);
						if (empty($query)) continue;

						$query = str_replace(PHP_EOL, '', $query);
						$query = str_replace('\r', '', $query);
						//echo \Lyrmin\Application\pre($query);

						\Lyrmin\Db\query($query);
					}
				}
			}

			Db\disconnect();
			\Lyrmin\Auth\pageRedirect();
		}
	}
}

function doUninstall() {
		foreach (['/mysql/uninstall.sql'] as $src) {
			$querySrc = trim(file_get_contents($_SERVER['DOCUMENT_ROOT'] . $src));
			preg_match_all('/^[A-z]+(.+\n|\;)+/mu', $querySrc, $arQuery);
			$arQuery = $arQuery[0];

			if (is_array($arQuery) && !empty($arQuery)) {
				foreach ($arQuery as $query) {
					$query = trim($query);
					if (empty($query)) continue;

					$query = str_replace(PHP_EOL, '', $query);
					$query = str_replace('\r', '', $query);
					\Lyrmin\Db\query($query);
				}
			}
		}

		$configFile = $_SERVER["DOCUMENT_ROOT"] . SITE_DIR . 'app/dbconnect.php';
		unlink($configFile) or die("Не могу удалить файл конфигурации");
		Db\disconnect();
		die("Приложение удалено");

	return true;
}