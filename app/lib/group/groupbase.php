<?

namespace Lyrmin\Group;

function getList($arFilter)
{
	$sql = [];

	$sql['SELECT'] = isset($arFilter['SELECT']) ? $arFilter['SELECT'] : '*';
	$sql['FROM'] = 'group';
	if (isset($arFilter['WHERE'])) {
		$sql['WHERE'] = $arFilter['WHERE'];
	}

	$query = \Lyrmin\Db\filterToQuery($sql);

	return \Lyrmin\Db\query($query);
}

function add()
{

}

function update()
{

}

function delete()
{

}