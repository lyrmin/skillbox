<?

namespace Lyrmin\Group;

use \Lyrmin\User, \Lyrmin\Group;

function getUserGroups($arFilter)
{
	$sql = [];
	$r = [];

	$sql['SELECT'] = isset($arFilter['SELECT']) ? $arFilter['SELECT'] : '*';
	$sql['FROM'] = 'user_group';
	if (isset($arFilter['WHERE'])) {
		$sql['WHERE'] = $arFilter['WHERE'];
	}

	$query = \Lyrmin\Db\filterToQuery($sql);
	$userGroups = \Lyrmin\Db\query($query);

	foreach ($userGroups as $arItem) {
		$r[] = isset($arFilter['WHERE']['GROUP_ID']) ? $arItem['USER_ID'] : $arItem['GROUP_ID'];
	}

	return $r;
}

function getUserRights($USER_ID = false)
{
	if (!intval($USER_ID)) return;

	$arUser = User\getList(['WHERE' => ['ID' => intval($USER_ID)]])[0];
	if (!empty($arUser)) {
		$arUser['GROUP'] = Group\getUserGroups(['WHERE' => ['USER_ID' => intval($USER_ID)]]);
		return in_array(2, $arUser['GROUP']) ? 'X' : 'R';
	}

	return false;
}
