<?php
/**
 * Created by PhpStorm.
 * User: las7
 * Date: 14.10.2018
 * Time: 14:07
 */

namespace Lyrmin\Menu;

function getFilePath()
{
	return __FILE__;
}

//function getMenuParams($set)
//{
//	static $arParams;
//	if (!empty($set) && is_array($set)) {
//		$arParams = $set;
//	}
//	return $arParams;
//}

function getMenu($menuName, $params = [])
{
	if (strlen($menuName) > 0) {
		//$arParams = getMenuParams($params);
		getMenuTemplate(getMenuArray($menuName, $params['KEY'], $params['SORT']), $menuName);
	}
}

function getMenuArray($menuName, $key, $sort)
{
	$arMenu = menu();
	if (strlen($menuName) > 0) {
		if(is_array($arMenu) && count($arMenu)) {
			usort($arMenu, function($a, $b) use ($key, $sort) {
				return $sort == 'DESC' ? $b[$key] <=> $a[$key] : $a[$key] <=> $b[$key];
			});
		}
	}

	return $arMenu;
}

function getMenuTemplate($arMenu = [], $menuName = '')
{
	if (!empty($arMenu) && strlen($menuName) > 0) {
		$currentUri = htmlspecialchars($_SERVER['REQUEST_URI']);
		\Lyrmin\Application\getTemplate('menu', $menuName, ["CURRENT_URL" => $currentUri, "ITEMS" => menu()]);
	}
	return false;
}

function menu($arMenuSet = [])
{
	static $arMenu;

	if (!empty($arMenuSet) && is_array($arMenuSet)) {
		$arMenu = $arMenuSet;
	}

	return $arMenu;
}