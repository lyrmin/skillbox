<?php
namespace Lyrmin\Message;

function getList($arFilter)
{
	$sql = [];

	$sql['SELECT'] = isset($arFilter['SELECT']) ? $arFilter['SELECT'] : '*';
	$sql['FROM'] = 'message';
	if (isset($arFilter['WHERE'])) {
		$sql['WHERE'] = $arFilter['WHERE'];
	}

	$query = \Lyrmin\Db\filterToQuery($sql);

	$arMessages = \Lyrmin\Db\query($query);

	if (is_array($arMessages) && !empty($arMessages)) {
		$arMessages = getMessageSection($arMessages);

	}

	return $arMessages;
}

function add($arFilter = [])
{
	if (empty($arFilter) || !is_array($arFilter)) return;

//	if (isset($arFilter["SECTION_ID"]) && !empty($arFilter["SECTION_ID"])) {
		//$sectionId = intval($arFilter["SECTION_ID"]);
		//unset($arFilter["SECTION_ID"]);
//	}

	$query = 'insert into message (`' . implode('`, `', array_keys($arFilter)) . '`) values (';
	$arr = [];

	foreach ($arFilter as $value) {
		if (empty($value)) {
			$arr[] = 'null';
		} else {
			$arr[] = "'" . $value . "'";
		}
	}

	$query .= implode(', ', $arr) . ')';

	$r = \Lyrmin\Db\query($query);

//	if ($r > 0 && $sectionId > 0) {
//		$query = 'insert into message_section (`MESSAGE_ID`, `SECTION_ID`) ';
//		$query .= "values ('" . $r . "', '" . $sectionId . "')";
//		\Lyrmin\Db\query($query);
//	}

	return $r;
}

function update($ID = 0, $arFields = [])
{
	if (empty(intval($ID)) || !is_array($arFields) || empty($arFields)) return;

	$sql = [];

	foreach ($arFields as $field => $value) {
		$sql[] = "`$field` = '$value'";
	}

	$query = "UPDATE `message` SET " . implode(', ', $sql) . " WHERE `message`.`ID` = " . intval($ID);

	$r = \Lyrmin\Db\query($query);

	return $r;
}

function delete()
{

}