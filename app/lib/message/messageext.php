<?

namespace Lyrmin\Message;

function isMessageAdded()
{
	global $_POST;

	$USER = \Lyrmin\User\getCurrent();

	if (
		isset($_POST['TITLE']) && isset($_POST['USER_TO']) && isset($_POST['MESSAGE']) && isset($_POST['SECTION_ID'])
	) {
		$arData = [
			'USER_FROM' => $USER['ID'],
			'USER_TO' => $_POST['USER_TO'],
			'TITLE' => $_POST['TITLE'],
			'MESSAGE' => $_POST['MESSAGE'],
			'SECTION_ID' => $_POST['SECTION_ID']
		];
		return add($arData);
	}

	return;
}

function getMessageSection($arMessage = []) {
	if (!is_array($arMessage)) return;

	$arSections = \Lyrmin\Section\getSections();
	$arNewMessage = [];

	foreach ($arMessage as $item) {
		$item["SECTION"] = $arSections[$item["SECTION_ID"]];
		$arNewMessage[$item['ID']] = $item;
	}

	return $arNewMessage;
}
