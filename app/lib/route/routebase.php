<?php
/**
 * Created by PhpStorm.
 * User: las7
 * Date: 15.10.2018
 * Time: 22:32
 */

namespace Lyrmin\Route;

function arUri($uri = '')
{
    static $arUri = [];
    if (strlen($uri)) {
        $arUri[] = $uri;
    }

    return $arUri;
}

function arPages($page = [])
{
    static $arPages = [];
    if (!empty($page)) {
        $arPages[] = $page;
    }

    return $arPages;
}

function add($data = [])
{
    if(is_array($data) && count($data) == 2) {
        $uri = $data[0];
        $pageCode = $data[1];
        arUri($uri);
        arPages($pageCode);
        return true;
    }

    return false;
}

function getCurrent()
{
    $currentUri = str_replace('index.php', '', htmlspecialchars($_SERVER['REQUEST_URI']));

    if (!empty($_SERVER['QUERY_STRING'])) {
		$currentUri = strstr($currentUri, '?', true);
	}

    if (in_array($currentUri, arUri())) {
        $id = array_search($currentUri, arUri());
        $arPages = arPages();
        return $arPages[$id];
    } else {
		header("HTTP/1.0 404 Not Found");
        return ['title' => 'Ошибка 404', 'html' => 'Внимание! Страница не найдена.'];
    }
}

function getList()
{
    $arUri = arUri();
    $r = [];
    foreach (arPages() as $key => $page) {
        $page['path'] = $arUri[$key];
        $r[] = $page;
    }
    return $r;
}