<?php
namespace Lyrmin\Section;

function getList($arFilter)
{
	$sql = [];

	$sql['SELECT'] = isset($arFilter['SELECT']) ? $arFilter['SELECT'] : '*';
	$sql['FROM'] = 'section';
	if (isset($arFilter['WHERE'])) {
		$sql['WHERE'] = $arFilter['WHERE'];
	}

	if (isset($arFilter['ORDER'])) {
		$sql['ORDER BY'] = $arFilter['ORDER'];
	}

	$query = \Lyrmin\Db\filterToQuery($sql);

	return \Lyrmin\Db\query($query);
}

function add()
{

}

function update()
{

}

function delete()
{

}