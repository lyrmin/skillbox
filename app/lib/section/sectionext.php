<?

namespace Lyrmin\Section;

function getSections($ID = 0) {
	$r = [];
	$arColors = [];
	$arSectionColor = [];
	$arSections = getList(["ORDER" => ["PARENT_ID" => "ASC", "ID" => "ASC"]]);

	foreach (\Lyrmin\Db\query('select * from `section_color`') as $color) {
		$arColors[$color['ID']] = $color['HEX'];
	}

	foreach ($arSections as $arSection) {
		$arSection['COLOR'] = $arColors[$arSection['COLOR']];
		$arGroupByParent[intval($arSection["PARENT_ID"])][$arSection["ID"]] = $arSection;
	}

	if (!empty($arGroupByParent)) {
		foreach ($arGroupByParent[0] as $arSectionPrimary) {
			$r[$arSectionPrimary['ID']] = $arSectionPrimary;

			if (key_exists($arSectionPrimary['ID'], $arGroupByParent)) {
				$arSection = $arGroupByParent[$arSectionPrimary['ID']];
				foreach ($arSection as $arItem) {
					$r[$arItem['ID']] = $arItem;
				}
			}
		}
	}

	return intval($ID) > 0 ? $r[intval($ID)] : $r;
}