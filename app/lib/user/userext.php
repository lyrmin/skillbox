<?
namespace Lyrmin\User;

use function Lyrmin\Auth\getLastLogin;
use Lyrmin\Group;

function getUsersByGroupId($GROUP_ID = 0) {
	if (!intval($GROUP_ID)) return false;

	$usersId = Group\getUserGroups(['WHERE' => ['GROUP_ID' => $GROUP_ID]]);
	return getList(['WHERE' => ['ID' => $usersId]]);
}

function getCurrent() {
	static $USER;
	if ($USER === null) {
		$USER = getList(['WHERE' => ['EMAIL' => getLastLogin()]]);

		if (count($USER)) {
			$USER = $USER[0];
			$USER['GROUP'] = Group\getUserGroups(['WHERE' => ['USER_ID' => $USER["ID"]]]);
		}
	}

	return $USER;
}