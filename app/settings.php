<?php
/**
 * Created by Aleksandr Lyrmin.
 * User: las7
 * Date: 14.10.2018
 * Time: 13:09
 * Файл настроек сайта
 */

// Настройки php
ini_set('display_errors', 'On');
error_reporting(E_ALL);

// Пользовательские настройки
$siteTemplate = 'standart';

// Пути сайта
define('SITE_DIR', '/');
$siteTemplatesPath = '/app/templates';
$siteLibrariesPath = '/app/lib';

// Настройки шаблона сайта
define('SITE_TEMPLATE_PATH', $siteTemplatesPath . SITE_DIR . $siteTemplate);

// Установка приложения
define(
	'INSTALL_FINISHED',
	file_exists($_SERVER["DOCUMENT_ROOT"] . SITE_DIR . 'app/dbconnect.php')
);

// Подключение библиотек
require_once('lib/db/dbinstall.php'); // Установка приложения
require_once('lib/db/dbbase.php'); // Подключение к БД
require_once('lib/db/dbext.php'); // Подключение к БД расширенные функции
require_once('lib/application/applicationbase.php'); // Приложение
require_once('lib/auth/authbase.php'); // Авторизация пользователя
require_once('lib/route/routebase.php'); // Роутинг страниц
require_once('lib/menu/menubase.php'); // Меню
require_once('lib/user/userbase.php'); // Пользователи
require_once('lib/user/userext.php'); // Пользователи расширенные функции
require_once('lib/group/groupbase.php'); // Группы пользователей
require_once('lib/group/groupext.php'); // Группы пользователей расширенные функции
require_once('lib/section/sectionbase.php'); // Разделы сообщений
require_once('lib/section/sectionext.php'); // Разделы сообщений расширенные функции
require_once('lib/message/messagebase.php'); // Сообщения
require_once('lib/message/messageext.php'); // Сообщения расширенные функции

// Настройки подключения к БД
if (INSTALL_FINISHED) {
	require_once('dbconnect.php');
	if (Lyrmin\Db\connect($dbHost, $dbName, $dbUser, $dbPass)) {
		\Lyrmin\Auth\isAuthorized();
	}
}

// Удаление приложения
if (isset($_REQUEST["UNINSTALL"]) && htmlspecialchars($_REQUEST["UNINSTALL"]) == "Y") {
	\Lyrmin\Install\doUninstall();
}