<?if (is_array($result["ITEMS"]) && count($result["ITEMS"])):?>
    <ul class="<?=$class?>">
        <?foreach($result["ITEMS"] as $arMenuItem):?>
        <li<?=$result["CURRENT_URL"] == $arMenuItem['path'] ? ' class="selected"' : ''?>><a href="<?=$arMenuItem['path']?>"><?=$arMenuItem['title']?></a></li>
        <?endforeach?>
    </ul>
<?endif?>