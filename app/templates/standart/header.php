<?php
/**
 * Created by PhpStorm.
 * User: las7
 * Date: 14.10.2018
 * Time: 13:09
 */

use Lyrmin\Menu;

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link href="<?=SITE_TEMPLATE_PATH?>/styles.css" rel="stylesheet" />
    <title>Project - ведение списков</title>

</head>
<body>
    <div class="header">
        <div class="logo"><img src="<?=SITE_TEMPLATE_PATH?>/i/logo.png" width="68" height="23" alt="Project" /></div>
        <div style="clear: both"></div>
    </div>
    <div class="menu mainMenu">
        <?Menu\getMenu('main_menu', ['KEY' => 'sort', 'SORT' => 'ASC'])?>
    </div>