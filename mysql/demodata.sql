insert into `user` (`ACTIVE`, `LAST_NAME`, `NAME`, `SECOND_NAME`, `EMAIL`, `PHONE`, `PASSWORD`)
    values (true, 'Администраторов', 'Администратор', 'Администраторович', 'test@lyrmin.ru', '79643429749', '40d21066d11a937b0e7834555f2c5f9a'),
    (true, 'Пользователев', 'Пользователь', 'Пользователич', 'las7@yandex.ru', '79643429749', '4efe04136ef5b83eea5985e617871bfa');

insert into `group` (`NAME`, `DESCRIPTION`)
    values ('Зарегистрированный пользователь', 'Зарегистрированный пользователь'),
    ('Пользователь имеющий право писать сообщения', 'Пользователь имеющий право писать сообщения');

insert into `user_group` (`USER_ID`, `GROUP_ID`)
    values (1, 2), (2, 1);

insert into `section_color` (`HEX`)
values ('#FF0000'),
       ('#00FF00'),
       ('#0000FF'),
       ('#F0F000'),
       ('#F00F00'),
       ('#F000F0'),
       ('#F0000F'),
       ('#0F0F00');

insert into `section` (`NAME`, `PARENT_ID`, `COLOR`, `CREATE_BY`)
    values ('Основные', NULL, 1, 1),
    ('Оповещения', NULL, 2, 1),
    ('Спам', NULL, 3, 1),
    ('по работе', 1, 4, 1),
    ('личные', 1, 5, 1),
    ('форумы', 2, 6, 1),
    ('магазины', 2, 7, 1),
    ('подписки', 2, 8, 1);

#