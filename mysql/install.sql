# Таблица пользователей +
create table if not exists `user` (
    `ID` int not null auto_increment,
    `ACTIVE` bool default true,
    `LAST_NAME` varchar(45) not null,
    `NAME` varchar(45) not null,
    `SECOND_NAME` varchar(45),
    `EMAIL` varchar(45) not null,
    `PHONE` varchar(16) not null,
    `PASSWORD` varchar(45) not null,
    primary key (`ID`)
) engine = InnoDB;

# Таблица групп пользователей +
create table if not exists `group` (
    `ID` int not null auto_increment,
    `NAME` varchar(45) not null,
    `DESCRIPTION` text not null,
    primary key (`ID`)
) engine = InnoDB;

# Таблица - связка пользователей и групп пользователей +
create table if not exists `user_group` (
    `USER_ID` int not null,
    `GROUP_ID` int not null,
    primary key (`USER_ID`, `GROUP_ID`),
    constraint `fk_user_group_user`
        foreign key (`USER_ID`)
        references `user` (`ID`)
        on delete cascade,
    constraint `fk_user_group_group`
        foreign key (`GROUP_ID`)
        references `group` (`ID`)
        on delete no action
) engine = InnoDB;

# Таблица цветов для разделов сообщений пользователей +
# Таблица переименована с `color` на `section_color` для удобства
create table if not exists `section_color` (
    `ID` int not null auto_increment,
    `HEX` varchar(7) not null unique,
    primary key (`ID`)
) engine = InnoDB;

# Таблица разделов сообщений пользователей +
create table if not exists `section` (
    `ID` int not null auto_increment,
    `NAME` varchar(45) not null,
    `PARENT_ID` int,
    `CREATE_DATE` datetime not null default CURRENT_TIMESTAMP,
    `COLOR` int not null,
    `CREATE_BY` int not null,
    primary key (`ID`, `COLOR`, `CREATE_BY`),
    constraint `fk_color`
        foreign key (`COLOR`)
        references `section_color` (`ID`)
        on delete no action,
    constraint `fk_section_user`
        foreign key (`CREATE_BY`)
        references `user` (`ID`)
        on delete no action
) engine = InnoDB;

# Таблица сообщений пользователей +
create table if not exists `message` (
    `ID` int not null auto_increment,
    `DATE_CREATE` datetime not null default CURRENT_TIMESTAMP,
    `USER_FROM` int not null,
    `USER_TO` int not null,
    `TITLE` varchar(45) not null,
    `MESSAGE` text not null,
    `READ_DATE` timestamp null default null,
    `SECTION_ID` int not null,
    primary key (`ID`, `USER_FROM`, `USER_TO`, `SECTION_ID`),
    constraint `fk_user_from`
        foreign key (`USER_FROM`)
        references `user` (`ID`)
        on delete no action,
    constraint `fk_user_to`
        foreign key (`USER_TO`)
        references `user` (`ID`)
        on delete no action,
    constraint `fk_section`
        foreign key (`SECTION_ID`)
        references `section` (`ID`)
        on delete no action
) engine = InnoDB;

SET FOREIGN_KEY_CHECKS = 1;

#