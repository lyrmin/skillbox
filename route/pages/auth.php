<?ob_start()?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="left-collum-index">
            <h1>Возможности проекта —</h1>
            <p>Вести свои личные списки, например покупки в магазине, цели, задачи и многое другое. Делится списками с друзьями и просматривать списки друзей.</p>
        </td>
        <td class="right-collum-index">
            <div class="project-folders-menu">
                <ul class="project-folders-v">
                    <li class="project-folders-v-active"><span>Авторизация</span></li>
                    <li><a href="#">Регистрация</a></li>
                    <li><a href="#">Забыли пароль?</a></li>
                </ul>
                <div style="clear: both;"></div>
            </div>
            <div class="index-auth">
                <?
                if (\Lyrmin\Application\isPost()) {
                    if (\Lyrmin\Auth\isAuthorized()) {
                        include $_SERVER['DOCUMENT_ROOT'] . '/include/success.php';
                    } else {
                        include $_SERVER['DOCUMENT_ROOT'] . '/include/error.php';
                    }
                }
                ?>
                <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
                    <?if (!\Lyrmin\Auth\isAuthorized()):?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <?if (!\Lyrmin\Auth\getLastLogin()):?>
                                <tr>
                                    <td class="iat">Ваш e-mail: <br /> <input type="text" id="login_id" size="30" name="login" value="" /></td>
                                </tr>
                            <?else:?>
                                <input type="hidden" name="login" value="<?=\Lyrmin\Auth\getLastLogin()?>">
                            <?endif?>
                            <tr>
                                <td class="iat">Ваш пароль: <br /> <input type="password" id="password_id" size="30" name="password" value="" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <?if (\Lyrmin\Auth\getLastLogin()):?>
                                        <input type="submit" name="another_user" value="Войти под другим пользователем" />
                                    <?endif?>
                                    <input type="submit" name="authorize" value="Войти" />
                                </td>
                            </tr>
                        </table>
                    <?else:?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td><input type="submit" name="logout" value="Выйти" /></td>
                            </tr>
                        </table>
                    <?endif?>
                </form>
            </div>
        </td>
    </tr>
</table>
<?return ob_get_clean()?>