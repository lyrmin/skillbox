<?ob_start()?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="left-collum-index">
            <h1>Возможности проекта —</h1>
        </td>
        <td class="right-collum-index">
            <div class="project-folders-menu">
                <ul class="project-folders-v">
                    <li class="project-folders-v-active"><span>Новое сообщение</span></li>
                </ul>
                <div style="clear: both;"></div>
            </div>
            <div class="index-auth">
                <?
                if (\Lyrmin\Application\isPost()) {
                    $r = \Lyrmin\Message\isMessageAdded();
                    if (intval($r) > 0) {
                        include $_SERVER['DOCUMENT_ROOT'] . '/include/message_add_success.php';
                    } else {
                        include $_SERVER['DOCUMENT_ROOT'] . '/include/message_add_error.php';
                    }
                }
                ?>
                <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="iat">Заголовок: <br /> <input type="text" size="30" name="TITLE" value="Сообщение <?=time()?>" /></td>
                        </tr>
                        <tr>
                            <td class="iat">Текст сообщения: <br /> <textarea name="MESSAGE" cols="106" rows="10"><?=time()?></textarea></td>
                        </tr>

                        <tr>
                            <td class="iat">Пользователь: <br />
                                <select name="USER_TO">
                                    <?foreach (\Lyrmin\User\getUsersByGroupId(2) as $arUser):?>
                                        <?if ($arUser["ID"] == $USER["ID"]) continue;?>
                                        <option value="<?=$arUser["ID"]?>"><?=$arUser["NAME"]?> <?=$arUser["SECOND_NAME"]?> <?=$arUser["LAST_NAME"]?> [<?=$arUser["EMAIL"]?>]</option>
                                    <?endforeach?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="iat1">Раздел сообщения: <br />
                                <? foreach (\Lyrmin\Section\getSections() as $arSection):?>
                                    <input type="radio" name="SECTION_ID" value="<?=$arSection["ID"]?>">&nbsp;<label style="background: <?=$arSection["COLOR"]?>; color: #FFF;">&nbsp;<?=$arSection["PARENT_ID"] ? "&ndash;&nbsp;":""?><?=$arSection["NAME"]?>&nbsp;</label>
                                    <br>
                                <?endforeach?>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <input type="submit" name="authorize" value="Отправить" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </td>
    </tr>
</table>
<?return ob_get_clean()?>