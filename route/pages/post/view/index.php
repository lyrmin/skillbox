<?ob_start()?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td class="left-collum-index">
            <h1>Список сообщений</h1>
            <?if (isset($_REQUEST["ID"]) && $_REQUEST["ID"] > 0):?>
                <?
				$r = \Lyrmin\Message\update(intval($_REQUEST["ID"]), ['READ_DATE' => date('Y-m-d H:i:s')]);
				$arMessage = \Lyrmin\Message\getList(['WHERE' => ['USER_TO' => $USER['ID'], 'ID' => intval($_REQUEST["ID"])]])[$_REQUEST["ID"]];
				$arUser = \Lyrmin\User\getList(["WHERE" => ["ID" => $arMessage["USER_FROM"]]])[0];
                ?>
                <h2><?=$arMessage["TITLE"]?> <?=$arMessage["DATE_CREATE"]?></h2>
                <p><?=$arMessage["MESSAGE"]?></p>
                <p>От: <?=$arUser["LAST_NAME"]?> <?=$arUser["NAME"]?> <?=$arUser["SECOND_NAME"]?> <?=$arUser["EMAIL"]?></p>
            <?else:?>
                <ol>
                    <?foreach (\Lyrmin\Message\getList(['WHERE' => ['USER_TO' => $USER['ID'], 'READ_DATE' => '']]) as $arMessage):?>
                        <li><a href="<?=$_SERVER['REDIRECT_URL'] . '?ID=' . $arMessage["ID"]?>"><?=$arMessage["TITLE"]?></a>&nbsp;[<?=$arMessage["SECTION"]["NAME"]?>]</li>
                    <?endforeach?>
                </ol>
            <?endif?>
        </td>
        <td class="right-collum-index">
            <div class="project-folders-menu hidden">
                <ul class="project-folders-v">
                    <li class="project-folders-v-active"><span>Новое сообщение</span></li>
                </ul>
                <div style="clear: both;"></div>
            </div>
            <div class="index-auth hidden">
                <form action="<?= $_SERVER['REQUEST_URI'] ?>" method="post">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td class="iat">Заголовок: <br /> <input type="text" size="30" name="TITLE" value="Сообщение <?=time()?>" /></td>
                        </tr>
                        <tr>
                            <td class="iat">Текст сообщения: <br /> <textarea name="MESSAGE" cols="106" rows="10"><?=time()?></textarea></td>
                        </tr>

                        <tr>
                            <td class="iat">Пользователь: <br />
                                <select name="TO">
                                    <?foreach (\Lyrmin\User\getUsersByGroupId(1) as $arUser):?>
                                        <?if ($arUser["ID"] == $USER["ID"]) continue;?>
                                        <option value="<?=$arUser["ID"]?>"><?=$arUser["NAME"]?> <?=$arUser["SECOND_NAME"]?> <?=$arUser["LAST_NAME"]?> [<?=$arUser["EMAIL"]?>]</option>
                                    <?endforeach?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="iat">Раздел сообщения: <br />
                                <select name="SECTION_ID">
                                    <? foreach (\Lyrmin\Section\getSections() as $arSection):?>
                                        <option value="<?=$arSection["ID"]?>"><?=$arSection["PARENT_ID"] ? "&ndash;&nbsp;":""?><?=$arSection["NAME"]?></option>
                                    <?endforeach?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <input type="submit" name="authorize" value="Отправить" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </td>
    </tr>
</table>
<?return ob_get_clean()?>